import { Injectable } from '@angular/core';
import { Lista } from '../../models';

/*
  Generated class for the DeseosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DeseosProvider {

  listas:Lista[] = [];

  constructor() {
    this.cargarStorage();
    // const lista1 = new Lista('Soy un pro');
    // const lista2 = new Lista('El mejor programador');

    // this.listas.push(lista1, lista2);

    // console.log(this.listas);
  }

  agregarLista(lista: Lista){
    this.listas.push(lista);

    this.guardarStorage();
  }

  guardarStorage(){
    localStorage.setItem('data', JSON.stringify(this.listas));
  }

  cargarStorage(){
    if(localStorage.getItem('data')){
      this.listas = JSON.parse(localStorage.getItem('data'));
    }else{
      this.listas = [];
    }
  }

  borrarLista(lista: Lista){
    this.listas = this.listas.filter( listaDate => {
      return listaDate.id != lista.id;
    });

    this.guardarStorage();
  }

}
