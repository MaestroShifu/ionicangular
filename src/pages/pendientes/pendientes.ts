import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { AgregarPage } from "../agregar/agregar";
import { DeseosProvider } from "../../providers/deseos/deseos";

/**
 * Generated class for the PendientesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pendientes',
  templateUrl: 'pendientes.html',
})
export class PendientesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public deseosProvider: DeseosProvider, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {

  }

  agregarlista(){
    const prompt = this.alertCtrl.create({
      title: 'Nueva Lista',
      message: "Nombre de la nueva lista que desea",
      inputs: [
        {
          name: 'titulo',
          placeholder: 'Nombre de la lista'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            // console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log(data);
            if(data.titulo.length == 0){
              return;
            }

            this.navCtrl.push(AgregarPage, {titulo: data.titulo});
          }
        }
      ]
    });
    prompt.present();
  }
}
