import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ItemSliding } from 'ionic-angular';

import { AgregarPage } from "../agregar/agregar";
import { DeseosProvider } from "../../providers/deseos/deseos";
import { Lista } from '../../models';

/**
 * Generated class for the ListasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listas',
  templateUrl: 'listas.html',
})
export class ListasPage {

  @Input()terminada: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public deseosProvider: DeseosProvider, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {

  }

  itemSelected(lista: Lista){
    console.log(lista);

    this.navCtrl.push(AgregarPage,  {titulo: lista.titulo, lista: lista});
  }

  borrarLista(lista :Lista){
    this.deseosProvider.borrarLista(lista);
  }

  editarLista(lista: Lista, slidingItem: ItemSliding){

    slidingItem.close();

    const prompt = this.alertCtrl.create({
      title: 'Editar nombre',
      message: "Editar el nombre de la lista",
      inputs: [
        {
          name: 'titulo',
          placeholder: 'Nombre de la lista',
          value: lista.titulo
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            // console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {

            if(data.titulo.length == 0){
              return;
            }
            //guarda por referencia
            lista.titulo = data.titulo;

            this.deseosProvider.guardarStorage();
          }
        }
      ]
    });
    prompt.present();
  }
}
