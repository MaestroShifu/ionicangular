import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DeseosProvider } from '../../providers/deseos/deseos';
import { Lista, ListaItem } from '../../models';

/**
 * Generated class for the AgregarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-agregar',
  templateUrl: 'agregar.html',
})
export class AgregarPage {

  lista:Lista;
  nombreItem:string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public deseosProvider:DeseosProvider) {

      if(this.navParams.get('lista')){
        this.lista = this.navParams.get('lista');
      }else{
        this.lista = new Lista(this.navParams.get('titulo'));
        this.deseosProvider.agregarLista(this.lista);
      }
  }

  ionViewDidLoad() {

  }

  agregarItem(){
    // console.log(this.nombreItem);
    if(this.nombreItem.length == 0){
      return;
    }

    const nuevoItem = new ListaItem(this.nombreItem);

    this.lista.items.push(nuevoItem);

    this.deseosProvider.guardarStorage();

    this.nombreItem = '';
  }

  completadoItem(item: ListaItem){
    item.completado = ! item.completado;

    const pendientes = this.lista.items.filter( iteamData =>{
      return !iteamData.completado;
    }).length;

    if(pendientes === 0){
      this.lista.terminada = true;
      this.lista.terminadaEn = new Date();
    } else {
      this.lista.terminada = false;
      this.lista.terminadaEn = null;
    }

    this.deseosProvider.guardarStorage();
  }

  deleteItem(index : number){
    //posicion a borrar y cuantos quieres borrar
    this.lista.items.splice(index, 1);

    this.deseosProvider.guardarStorage();
  }

}
